package cmd

import (
	"fmt"
	"log"
	"path/filepath"

	"github.com/spf13/cobra"
	"github.com/thekad/aoc/2022/cmd/eleven"
	"github.com/thekad/aoc/2022/pkg/file"
)

var elevenCmd = &cobra.Command{
	Use:   "eleven",
	Short: "Run the eleventh day's exercises",
	Run: func(cmd *cobra.Command, args []string) {
		var filePath string
		var err error

		if len(args) == 0 {
			filePath, err = file.DataFilePath("day-11.txt")
			if err != nil {
				log.Fatal(err)
			}
		} else {
			filePath, err = filepath.Abs(args[0])
			if err != nil {
				log.Fatal(err)
			}
		}
		log.Println(fmt.Sprintf("Loading file %s", filePath))

		chunks, err := file.ReadChunks(filePath, "\n\n")

		if err != nil {
			log.Fatal(err)
		}

		eleven.Main(chunks)
	},
}

func init() {
	rootCmd.AddCommand(elevenCmd)
}
