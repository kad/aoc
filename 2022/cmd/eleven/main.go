package eleven

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/davecgh/go-spew/spew"
	"github.com/thekad/aoc/2022/pkg/queue"
)

const (
	plus   string = "+"
	times  string = "*"
	factor string = "**"
)

type operation struct {
	operator string
	operand  int
}

func (o *operation) String() string {
	switch o.operator {
	case plus:
		return fmt.Sprintf("increases by %d", o.operand)
	case times:
		return fmt.Sprintf("is multiplied by %d", o.operand)
	case factor:
		return fmt.Sprintf("is factored by %d", o.operand)
	}

	return ""
}

type monkey struct {
	items   queue.IntQueue
	oper    operation
	divTest int
	ifTrue  int
	ifFalse int
}

func (o *operation) execute(worry int) int {
	switch o.operator {
	case plus:
		return o.operand + worry
	case times:
		return o.operand * worry
	case factor: // "special" case
		return worry * worry
	}
	return -1
}

// Main method for cmd "eleven"
func Main(chunks [][]string) {
	monkeys := []monkey{}

	for _, chunk := range chunks {
		m := monkey{
			items: *queue.NewIntQueue(),
		}
		for i := 1; i < len(chunk); i++ {
			line := strings.TrimSpace(chunk[i])

			// starting items
			prefix := "Starting items: "
			if strings.HasPrefix(line, prefix) {
				for _, i := range strings.Split(strings.TrimPrefix(line, prefix), ", ") {
					item, _ := strconv.Atoi(i)
					m.items.Add(item)
				}
				continue
			}

			// operation
			prefix = "Operation: new = old "
			if strings.HasPrefix(line, prefix) {
				parts := strings.Split(strings.TrimPrefix(line, prefix), " ")
				var operator string
				var operand int
				// "special" case
				if parts[0] == "*" && parts[1] == "old" {
					operator = "**"
					operand = 2
				} else {
					operator = parts[0]
					operand, _ = strconv.Atoi(parts[1])
				}
				m.oper = operation{
					operator: operator,
					operand:  operand,
				}
				continue
			}

			// test
			prefix = "Test: divisible by "
			if strings.HasPrefix(line, prefix) {
				m.divTest, _ = strconv.Atoi(strings.TrimPrefix(line, prefix))
				continue
			}

			// if true
			prefix = "If true: throw to monkey "
			if strings.HasPrefix(line, prefix) {
				s := strings.TrimPrefix(line, prefix)
				m.ifTrue, _ = strconv.Atoi(s)
				continue
			}

			// if false
			prefix = "If false: throw to monkey "
			if strings.HasPrefix(line, prefix) {
				s := strings.TrimPrefix(line, prefix)
				m.ifFalse, _ = strconv.Atoi(s)
				continue
			}
		}

		monkeys = append(monkeys, m)
	}

	// part 1
	inspections := map[int]int{}
	for round := 1; round < 21; round++ {
		for idx, monkey := range monkeys {
			fmt.Println(fmt.Sprintf("Monkey %d:", idx))
			for monkey.items.Size() > 0 {
				item := *monkey.items.Remove()
				inspections[idx]++
				fmt.Println(fmt.Sprintf("  Monkey inspects an item with worry level of %d.", item))
				worry := monkey.oper.execute(item)
				fmt.Println(fmt.Sprintf("    Worry level %s to %d.", monkey.oper.String(), worry))
				worry = worry / 3
				fmt.Println(fmt.Sprintf("    Monkey gets bored with item. Worry level is divided by 3 to %d.", worry))
				if worry%monkey.divTest == 0 {
					fmt.Println(fmt.Sprintf("    Current worry level is divisible by %d.", monkey.divTest))
					fmt.Println(fmt.Sprintf("    Item with worry level %d is thrown to monkey %d.", worry, monkey.ifTrue))
					monkeys[monkey.ifTrue].items.Add(worry)
				} else {
					fmt.Println(fmt.Sprintf("    Current worry level is not divisible by %d.", monkey.divTest))
					fmt.Println(fmt.Sprintf("    Item with worry level %d is thrown to monkey %d.", worry, monkey.ifFalse))
					monkeys[monkey.ifFalse].items.Add(worry)
				}
			}
		}
		fmt.Println(fmt.Sprintf("After round %d, the monkeys are holding items with these worry levels:", round))
		for idx, monkey := range monkeys {
			fmt.Print(fmt.Sprintf("Monkey %d: ", idx))
			fmt.Println(fmt.Sprintf("%v", monkey.items))
		}
	}
	spew.Dump(inspections)
}
