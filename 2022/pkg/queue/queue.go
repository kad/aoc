package queue

// IntQueue is a simple queue implementation of type int
type IntQueue struct {
	elements []int
}

// NewIntQueue is the constructor
func NewIntQueue() *IntQueue {
	rs := IntQueue{
		elements: []int{},
	}

	return &rs
}

// Add adds an element to the end of the array
func (q *IntQueue) Add(e int) {
	q.elements = append(q.elements, e)
}

// Size returns the size of the array
func (q *IntQueue) Size() int {
	return len(q.elements)
}

// Remove gets the element from the front of the array
func (q *IntQueue) Remove() *int {
	if q.Size() == 0 {
		return nil
	}
	r := q.elements[0]
	q.elements = q.elements[1:]

	return &r
}

// Peek looks at the next item in the queue without removing it
func (q *IntQueue) Peek() *int {
	if q.Size() == 0 {
		return nil
	}
	return &q.elements[0]
}
