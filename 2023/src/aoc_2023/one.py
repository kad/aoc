import aoc_2023


class One(aoc_2023.Base):
    day: str = "One"
    default_input: str = "data/one.txt"

    def get_digits(self, ln: str) -> str:
        digits: str = ""
        table: tuple[str] = (
            "zero",
            "one",
            "two",
            "three",
            "four",
            "five",
            "six",
            "seven",
            "eight",
            "nine",
        )

        for x in range(len(ln)):
            if ln[x].isdigit():
                digits += ln[x]
                continue
            for digit, word in enumerate(table):
                if ln.startswith(word, x):
                    digits += str(digit)

        return digits

    def borders(self, ln: str) -> int:
        first: int = 0
        last: int = 0
        # forwards
        for c in ln:
            if c.isdigit():
                first = int(c)
                break
        # backwards
        for c in ln[::-1]:
            if c.isdigit():
                last = int(c)
                break

        return int(f"{first}{last}")

    def __init__(self, input: str = None):
        super().__init__(input)

    def run(self, part: int):
        if part in [0, 1]:
            total: int = 0
            for ln in self.input.splitlines():
                total += self.borders(ln)

            with self.part("one"):
                print(f"total is {total}")

        if part in [0, 2]:
            total = 0
            for ln in self.input.splitlines():
                newline = self.get_digits(ln)
                n: int = self.borders(newline)
                total += n

            with self.part("two"):
                print(f"total is {total}")
