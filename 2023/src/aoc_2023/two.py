import aoc_2023


class Game:
    id: int
    reveals: list[dict[str, int]]
    limits: dict[str, int] = {
        "red": 12,
        "green": 13,
        "blue": 14,
    }

    def __init__(self, id: int, line: str, limits: dict[str, int] = None):
        self.id = id
        self.reveals = []
        if limits is not None:
            self.limits = limits
        for reveal in [_.strip() for _ in line.split(";")]:
            for subset in [_.strip() for _ in reveal.split(",")]:
                self.reveals.append({subset.split(" ")[1]: int(subset.split(" ")[0])})

    @property
    def possible(self) -> bool:
        for r in self.reveals:
            for color, qty in r.items():
                if qty > self.limits[color]:
                    return False

        return True

    def __repr__(self) -> str:
        return f"<Game {self.id}, possible? {self.possible}>"


class Two(aoc_2023.Base):
    day: str = "Two"
    default_input: str = "data/two.txt"

    def __init__(self, input: str = None):
        super().__init__(input)

    def run(self, part: int):
        # load games
        game: int = 1
        games: list[Game] = []
        for ln in self.input.splitlines():
            games.append(Game(game, ln.split(":")[-1].strip()))
            game += 1

        if part in [0, 1]:
            possibles: int = 0
            for g in games:
                possibles += g.id if g.possible else 0
                game += 1

            with self.part("one"):
                print(f"Possible games: {possibles}")

        if part in [0, 2]:
            powers: int = 0
            for g in games:
                maxes: dict[str, int] = {
                    "red": 0,
                    "green": 0,
                    "blue": 0,
                }
                for reveal in g.reveals:
                    for color, qty in reveal.items():
                        if qty > maxes[color]:
                            maxes[color] = qty
                powers += maxes["red"] * maxes["green"] * maxes["blue"]

            with self.part("two"):
                print(f"Sum of powers is {powers}")
