import os.path
import contextlib


def header(title):
    print("=" * len(title))
    print(title)
    print("=" * len(title))


class Base:
    day: str = None
    default_input: str = None
    input: str = None

    @contextlib.contextmanager
    def part(self, which: str):
        title = f"Day {self.day} part {which}"
        header(title)
        try:
            yield
        finally:
            print("-" * len(title))
            print()

    def __init__(self, input: str = None):
        if self.day is None:
            raise NotImplementedError("No day defined")

        f = os.path.abspath(self.default_input if input is None else input)

        if f is None:
            raise NotImplementedError("No input defined")

        with open(f) as fh:
            self.input = fh.read()

    def run(self, part: int):
        raise NotImplementedError
