import sys
import docopt

import aoc_2023.one as one
import aoc_2023.two as two


def main():
    usage: str = """Advent of Code 2023.

    Usage:
        aoc-2023 <day> [--part=<pt>] [PATH]
        aoc-2023 --version

    Options:
        -h --help       Print usage.
        --version       Print version.
        --part=<pt>     Which part you want to run defaults all [default: 0]
    """

    args = docopt.docopt(usage, version="Advent of Code 2023")
    match args["<day>"].lower():
        case "one":
            ck = one.One(args["PATH"])
        case "two":
            ck = two.Two(args["PATH"])
        case _:
            raise NotImplementedError(f"Not implemented: {args['<day>']}")

    return ck.run(int(args["--part"]))


if __name__ == "__main__":
    sys.exit(main())
