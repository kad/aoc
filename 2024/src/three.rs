use regex::Regex;
use std::fs;
use std::num::ParseIntError;
use std::path::PathBuf;

pub fn cmd(path: PathBuf) -> Result<(), ParseIntError> {
    let instructions = fs::read_to_string(path).unwrap();

    // Part 1
    let re = Regex::new(r"mul\((?<left>\d+),(?<right>\d+)\)").unwrap();
    let mut sums: Vec<u32> = Vec::new();
    for cap in re.captures_iter(&instructions) {
        let left = cap.name("left").unwrap().as_str().parse::<u32>()?;
        let right = cap.name("right").unwrap().as_str().parse::<u32>()?;
        // println!("{:?} ~ {:?}x{:?}", cap, left, right,);
        sums.push(left * right);
    }
    println!("Sum of instructions is {:?}", sums.iter().sum::<u32>());

    // Part 2
    sums = Vec::new();
    let re =
        Regex::new(r"(?<dont>don't\(\))|(?<do>do\(\))|mul\((?<left>\d+),(?<right>\d+)\)").unwrap();
    let mut enable: bool = true;
    for cap in re.captures_iter(&instructions) {
        if Option::is_some(&cap.name("dont")) {
            // println!("disable next");
            enable = false;
            continue;
        }
        if Option::is_some(&cap.name("do")) {
            // println!("enable next");
            enable = true;
            continue;
        }
        if enable {
            let left = cap.name("left").unwrap().as_str().parse::<u32>()?;
            let right = cap.name("right").unwrap().as_str().parse::<u32>()?;
            sums.push(left * right);
            // println!("Add {}x{}={}", left, right, left * right);
        }
    }
    println!("Conditional sum of instructions is {:?}", sums.iter().sum::<u32>());

    Ok(())
}
