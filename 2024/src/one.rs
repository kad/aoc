use std::num::ParseIntError;
use std::path::PathBuf;

pub fn cmd(path: PathBuf) -> Result<(), ParseIntError> {
    let mut left: Vec<i32> = Vec::new();
    let mut right: Vec<i32> = Vec::new();

    if let Ok(lines) = crate::io::read_lines(path) {
        // read both columns
        for line in lines {
            let numbers: Vec<&str> = line.split_whitespace().collect();
            left.push(numbers[0].parse()?);
            right.push(numbers[1].parse()?);
        }
        left.sort();
        right.sort();

        // Part 1
        let mut distance = 0;
        for idx in 0..left.len() {
            distance += (left[idx] - right[idx]).abs();
        }
        println!("Total distance is {}", distance);

        // Part 2
        let mut similarity_score = 0;
        for needle in left {
            let occurrences = right.iter().filter(|&v| *v == needle).count() as i32;
            let similarity = needle * occurrences;
            similarity_score += similarity;
        }
        println!("Similarity score is {}", similarity_score);
    }

    Ok(())
}
