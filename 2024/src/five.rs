use std::fs;
use std::num::ParseIntError;
use std::path::PathBuf;

#[derive(Debug, Clone)]
struct Update {
    pages: Vec<u32>,
}

impl Update {
    fn middle(&self) -> u32 {
        *self.pages.get(self.pages.len() / 2).unwrap()
    }

    fn copy(&self) -> Self {
        Self {
            pages: self.pages.clone(),
        }
    }
}

#[derive(Debug, Clone)]
struct Rule {
    before: u32,
    after: u32,
}

impl Rule {
    fn comply(&self, u: &Update) -> bool {
        let b = u.pages.iter().position(|x| x == &self.before).unwrap();
        let a = u.pages.iter().position(|x| x == &self.after).unwrap();

        b <= a
    }

    fn matches(&self, u: &Update) -> bool {
        u.pages.contains(&self.before) && u.pages.contains(&self.after)
    }

    fn fix(&self, u: &Update) -> Update {
        let mut r = u.pages.clone();
        let b = r.iter().position(|x| x == &self.before).unwrap();
        let a = r.iter().position(|x| x == &self.after).unwrap();

        // flip them
        if a < b {
            r.swap(a, b);
        }

        Update { pages: r }
    }
}

struct RuleSet {
    rules: Vec<Rule>,
}

impl RuleSet {
    fn comply(&self, u: &Update) -> bool {
        let mut valid: bool = true;
        for r in &self.rules {
            if r.matches(u) && !r.comply(u) {
                valid = false;
                break;
            }
        }

        valid
    }
}

pub fn cmd(path: PathBuf) -> Result<(), ParseIntError> {
    if let Ok(data) = fs::read_to_string(path) {
        let parts = data.split("\n\n").collect::<Vec<_>>();

        let rules: Vec<Rule> = parts
            .first()
            .unwrap()
            .split("\n")
            .map(|r| {
                let t = r.split("|").map(String::from).collect::<Vec<String>>();
                Rule {
                    before: t.first().unwrap().parse().unwrap(),
                    after: t.get(1).unwrap().parse().unwrap(),
                }
            })
            .collect();
        let ruleset: RuleSet = RuleSet { rules };

        let updates: Vec<Update> = parts
            .get(1)
            .unwrap()
            .split_whitespace()
            .map(|u| Update {
                pages: u
                    .split(",")
                    .map(|s| String::from(s).parse().unwrap())
                    .collect(),
            })
            .collect();

        // Part 1
        let mut middles: Vec<u32> = vec![];
        let mut failures: Vec<Update> = vec![];
        for update in &updates {
            let mut valid: bool = true;

            for rule in &ruleset.rules {
                if rule.matches(update) {
                    if rule.comply(update) {
                        valid = true;
                    } else {
                        failures.push(update.copy());
                        valid = false;
                        break;
                    }
                }
            }

            if valid {
                middles.push(update.middle());
            }
        }
        println!(
            "Valid updates middle sum: {:?}",
            middles.iter().sum::<u32>()
        );

        // Part 2
        middles = vec![];
        for update in &failures {
            let mut to_fix = update.copy();
            while !ruleset.comply(&to_fix) {
                for rule in &ruleset.rules {
                    if rule.matches(&to_fix) && !rule.comply(&to_fix) {
                        to_fix = rule.fix(&to_fix);
                    }
                }
            }
            middles.push(to_fix.middle());
        }
        println!(
            "Fixed updates middle sum: {:?}",
            middles.iter().sum::<u32>()
        );
    }

    Ok(())
}
