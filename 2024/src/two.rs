use std::cmp::Ordering;
use std::num::ParseIntError;
use std::path::PathBuf;

fn check_safety(numbers: &[i32]) -> bool {
    let asc = numbers.windows(2).all(|n| n[0] <= n[1]);
    let desc = numbers.windows(2).all(|n| n[0] >= n[1]);

    let mut is_safe = true;
    for idx in 1..numbers.len() {
        let prev = numbers[idx - 1];
        let curr = numbers[idx];
        let diff = (curr - prev).abs();
        is_safe = match curr.cmp(&prev) {
            Ordering::Equal => false,
            Ordering::Greater => asc && diff < 4,
            Ordering::Less => desc && diff < 4,
        };
        if !is_safe {
            return false;
        }
    }

    is_safe
}

fn check_safety_with_dampener(numbers: &[i32]) -> bool {
    if check_safety(numbers) {
        return true;
    }

    // if it wasn't safe, retry by eliminating levels 1 by 1
    for n in 0..numbers.len() {
        let slice = [&numbers[0..n], &numbers[n + 1..]].concat();
        if check_safety(&slice) {
            return true;
        }
    }

    false
}

pub fn cmd(path: PathBuf) -> Result<(), ParseIntError> {
    let mut vectors: Vec<Vec<i32>> = Vec::new();
    if let Ok(lines) = crate::io::read_lines(path) {
        // Read the data into a 2 dimensional vector
        for line in lines {
            let ns: Vec<i32> = line
                .split_whitespace()
                .map(|x| x.parse::<i32>().unwrap())
                .collect();
            vectors.push(ns);
        }

        // Part 1
        let mut safes = 0;
        let mut unsafes = 0;
        for numbers in &vectors {
            let is_safe = check_safety(numbers);
            match is_safe {
                true => safes += 1,
                false => unsafes += 1,
            }
            println!("{:?} ? {}", numbers, is_safe);
        }
        println!("Safe numbers: {}, unsafe numbers: {}", safes, unsafes);

        // Part 2
        safes = 0;
        unsafes = 0;
        for numbers in &vectors {
            let is_safe = check_safety_with_dampener(numbers);
            match is_safe {
                true => safes += 1,
                false => unsafes += 1,
            }
            println!("{:?} ? {}", numbers, is_safe);
        }
        println!("Safe numbers: {}, unsafe numbers: {}", safes, unsafes);
    }

    Ok(())
}
