use std::num::ParseIntError;
use std::path::PathBuf;

fn find_xmas(input: Vec<Vec<char>>) -> usize {
    let mut found = 0;
    for line in input {
        let line = line.into_iter().collect::<String>().to_uppercase();
        found += line.match_indices("XMAS").count();
    }

    found
}

#[derive(Debug)]
struct Point {
    x: usize,
    y: usize,
}

impl Point {
    fn left(&self) -> usize {
        &self.x - 1
    }
    fn right(&self) -> usize {
        &self.x + 1
    }
    fn up(&self) -> usize {
        &self.y - 1
    }
    fn down(&self) -> usize {
        &self.y + 1
    }
}

fn find_x_mas(input: Vec<Vec<char>>) -> usize {
    let mut ct = 0;

    // only need to traverse the section of the map that can fit the word
    for x in 1..input[0].len() - 1 {
        for y in 1..input.len() - 1 {
            let here = input[x][y];
            if here == 'A' {
                let point = Point { x, y };
                let ul = input[point.left()][point.up()];
                let dr = input[point.right()][point.down()];
                let dl = input[point.left()][point.down()];
                let ur = input[point.right()][point.up()];
                let mut id = 0;
                // l-r down
                if ul == 'M' && dr == 'S' {
                    id += 1;
                }
                // r-l down
                if ur == 'M' && dl == 'S' {
                    id += 1;
                }
                // r-l up
                if dl == 'M' && ur == 'S' {
                    id += 1;
                }
                // l-r up
                if dr == 'M' && ul == 'S' {
                    id += 1;
                }
                if id == 2 {
                    //println!("{:?} {:?}A{:?}, {:?}A{:?}", point, ul, dr, dl, ur,);
                    ct += 1;
                }
            }
        }
    }

    ct
}

fn diagonal(input: &[Vec<char>], col: usize, row: usize) -> Vec<char> {
    let mut x = row;
    let mut y = col;
    let mut r = vec![];
    loop {
        if y >= input.len() {
            break;
        }
        if x >= input[y].len() {
            break;
        }
        r.push(input[x][y]);
        x += 1;
        y += 1;
    }

    r
}

fn diagonals(input: &[Vec<char>]) -> Vec<Vec<char>> {
    let mut r = vec![];

    let mut y = input.len() - 1;
    while y > 0 {
        let d = diagonal(input, 0, y);
        if d.len() >= "XMAS".len() {
            r.push(d);
        }
        y -= 1;
    }

    let mut x = 0;
    while x < input[0].len() {
        let d = diagonal(input, x, 0);
        if d.len() >= "XMAS".len() {
            r.push(d);
        }
        x += 1;
    }

    r
}

fn verticals(input: Vec<String>) -> Vec<Vec<char>> {
    let mut r: Vec<Vec<char>> = Vec::new();
    let mut v: Vec<char> = Vec::new();

    for x in 0..input[0].len() {
        for y in &input {
            v.push(y.chars().nth(x).unwrap());
        }
        r.push(v.clone());
        v = vec![];
    }

    r
}

pub fn cmd(path: PathBuf) -> Result<(), ParseIntError> {
    if let Ok(data) = crate::io::read_lines(path) {
        // first part
        let lr_down: Vec<Vec<char>> = data.iter().map(|s| s.chars().collect()).collect();
        let lr_down_diags: Vec<Vec<char>> = diagonals(&lr_down);
        let lr_down_diags_rev: Vec<Vec<char>> = lr_down_diags
            .iter()
            .map(|x| x.clone().iter().rev().copied().collect())
            .collect();
        let rl_down: Vec<Vec<char>> = data.iter().map(|s| s.chars().rev().collect()).collect();
        let rl_down_diags: Vec<Vec<char>> = diagonals(&rl_down);
        let rl_down_diags_rev: Vec<Vec<char>> = rl_down_diags
            .iter()
            .map(|x| x.clone().iter().rev().copied().collect())
            .collect();
        let verts = verticals(data);
        let verts_rev: Vec<Vec<char>> = verts
            .iter()
            .map(|x| x.clone().iter().rev().copied().collect())
            .collect();

        let mut found = 0;
        for input in [
            &lr_down,
            &lr_down_diags,
            &lr_down_diags_rev,
            &rl_down,
            &rl_down_diags,
            &rl_down_diags_rev,
            &verts,
            &verts_rev,
        ] {
            /*
            for line in &input {
                println!("{}", line.iter().collect::<String>());
            }
            println!("========================");
            */
            found += find_xmas(input.to_vec());
        }
        println!("Total XMAS found: {}", found);

        // second part
        let ct = find_x_mas(lr_down.to_vec());
        println!("Total X-MAS found: {}", ct);
    }

    Ok(())
}
