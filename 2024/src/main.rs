mod five;
mod four;
mod io;
mod one;
mod three;
mod two;
use std::{num::ParseIntError, path::PathBuf};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
enum App {
    #[structopt(name = "one", about = "Runs the first day's exercise(s)")]
    One {
        #[structopt(parse(from_os_str))]
        #[structopt(default_value = "data/day1.txt")]
        path: PathBuf,
    },
    #[structopt(name = "two", about = "Runs the second day's exercise(s)")]
    Two {
        #[structopt(parse(from_os_str))]
        #[structopt(default_value = "data/day2.txt")]
        path: PathBuf,
    },
    #[structopt(name = "three", about = "Runs the third day's exercise(s)")]
    Three {
        #[structopt(parse(from_os_str))]
        #[structopt(default_value = "data/day3.txt")]
        path: PathBuf,
    },
    #[structopt(name = "four", about = "Runs the fourth day's exercise(s)")]
    Four {
        #[structopt(parse(from_os_str))]
        #[structopt(default_value = "data/day4.txt")]
        path: PathBuf,
    },
    #[structopt(name = "five", about = "Runs the fifth day's exercise(s)")]
    Five {
        #[structopt(parse(from_os_str))]
        #[structopt(default_value = "data/day5.txt")]
        path: PathBuf,
    },
}

fn main() -> Result<(), ParseIntError> {
    let args = App::from_args();
    match args {
        App::One { path } => one::cmd(path),
        App::Two { path } => two::cmd(path),
        App::Three { path } => three::cmd(path),
        App::Four { path } => four::cmd(path),
        App::Five { path } => five::cmd(path),
    }
}
